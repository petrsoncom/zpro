# Základy programování (ZPRO)

Toto cvičení je určeno především posluchačům, kteří nemají žádné, nebo jen malé zkušenosti s programováním. Seznámí posluchače se základními pojmy v oblasti moderních informačních technologiích a s programovacím jazykem `C++`. **ZPRO** na žádné předměty nenavazuje předpokládá se pouze základní uživatelská znalost počítače. Za cíl se klade naučit posluchače řešit základní programátorské úkoly s pomocí jazyka `C++`.

**Oficiální Osnova:**	
1. Co je to počítač, co je to program, co je algoritmus
2. Zobrazování dat v paměti počítače, význam datových typů
3. Struktura programu
4. Proměnné a neobjektové datové typy
5. Příkazy, Vstupní a výstupní operace
6. Funkce
7. Ukazatele, spojové seznamy
8. Modulární stavba programu, objektové typy
 

**Osnova cvičení:**	
1. Motivace programování. Ukázky z reálného světa. 
2. Základní myšlenky fungování počítače. Algoritmy a software.
3. Způsoby vývoje programů, nastavení vývojového prostředí, Git.
4. Struktura programu v `C++`. Paměť, datové typy a jejich využití. 
5. Vstupy a výstupy a práce se soubory.
6. Větvení, logické operace a cykly.
7. Základy tříd a objektového programování.
8. Ukazatele, třídy s ukazatelmi.
9. Nástroje na správu kódu, git a workflow.
10. Oborově zameřené aplikace programování v `C++`.
11. Jiné programovací jazyky a nástroje: `Python`, `LaTex`, `Matlab` a `Mathematica`.


**Doporučená literatura:** 
* Virius, M.: Základy programování v C++. Praha: ČVUT 2014. ISBN 978-80-01-05470-3.
* Pro zájemce: [introductory kurz do C++](https://www.codecademy.com/learn/learn-c-plus-plus). 
Doporučujeme lidem, co mají ZPRO povinně.